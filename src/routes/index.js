//Import Routes
const usersRoutes = require("./user");
const router = require("express").Router;


//Module exports
module.exports = {
  usersRoutes,
};
